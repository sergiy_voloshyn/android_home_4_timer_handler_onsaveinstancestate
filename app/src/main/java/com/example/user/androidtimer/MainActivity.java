package com.example.user.androidtimer;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity

{

    Fragment timerFrag;
    Fragment stopwatchFrag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ButterKnife.bind(this);
        timerFrag = TimerFragment.newInstance();
        stopwatchFrag = StopwatchFragment.newInstance();

    }

    @OnClick(R.id.timer_btn)
    public void onClickTimer() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, timerFrag)
                .commit();

    }

    @OnClick(R.id.stopwatch_btn)
    public void onClickStopwatch() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, stopwatchFrag)
                .commit();

    }

}

