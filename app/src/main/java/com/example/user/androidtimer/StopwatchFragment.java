package com.example.user.androidtimer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;

/*
Управление для секундомера
- один клик по цифре - запускает/возобновляет секундомер, или, если секундомер уже запущен, приостанавливает.
- длинный клик (longClick) - останавливает и сбрасывает секундомер.

 */
public class StopwatchFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.stopwatchLabel)
    TextView stopwatchLabel;
    public static final int START = 1;
    public static final int STOP = 2;
    public static final String STOPWATCH_DATA = "TIMER_DATA";
    public static final String STOPWATCH_START = "TIMER_START";

    int time = 0;
    boolean isStarted;
    private Bundle outState;

    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {
                stopwatchLabel.setText(String.valueOf(time));
                time++;
                sendEmptyMessageDelayed(START, 1000);

            } else if (msg.what == STOP) {
                stopTimer();
            }

        }
    };


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(STOPWATCH_DATA, time);

        if (isStarted) outState.putBoolean(STOPWATCH_START, true);
        else outState.putBoolean(STOPWATCH_START, false);

        this.outState = outState;

    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {

            time = savedInstanceState.getInt(STOPWATCH_DATA);

            if (savedInstanceState.getBoolean(STOPWATCH_START)) {
                startTimer();
            } else {

                stopTimer();
            }
        }
    }


    public static StopwatchFragment newInstance() {
        StopwatchFragment fragment = new StopwatchFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        unbinder.unbind();
        stopTimer();
    }

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);
    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }

    @OnClick(R.id.stopwatchLabel)
    public void onNumberClick() {
        if (isStarted) {
            stopTimer();
        } else {
            startTimer();
        }
    }

    @OnLongClick(R.id.stopwatchLabel)
    public boolean onLongClick() {
        h.sendEmptyMessage(STOP);
        time = 0;
        stopwatchLabel.setText(String.valueOf(time));
        return true;

    }


}
