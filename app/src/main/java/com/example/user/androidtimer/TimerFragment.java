package com.example.user.androidtimer;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.BinderThread;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Unbinder;

/*
Управление для таймера
- один клик по цифре - запускает/возобновляет отсчет на таймере, или, если таймер уже запущен, приостанавливает.
- длинный клик (longClick) - останавливает и сбрасывает таймер.


 */
public class TimerFragment extends Fragment {

    public static final int START = 1;
    public static final int STOP = 2;
    public static final String TIMER_DATA = "TIMER_DATA";
    public static final String TIMER_START = "TIMER_START";
    public static final int TIMER_TIME = 30;
    boolean isStarted;
    public int time = TIMER_TIME;

    //for saved state
    int timerSaved = TIMER_TIME;
    private Bundle outState;

    Unbinder unbinder;
    @BindView(R.id.timerLabel)
    TextView timerLabel;



    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {

                if (time >= 0) {
                    timerLabel.setText(String.valueOf(time));
                    time--;
                    sendEmptyMessageDelayed(START, 1000);
                }
            } else if (msg.what == STOP) {
                stopTimer();
            }
        }
    };
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(TIMER_DATA, time);

        if (isStarted) outState.putBoolean(TIMER_START, true);
        else outState.putBoolean(TIMER_START, false);

        this.outState = outState;
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {

            time = savedInstanceState.getInt(TIMER_DATA);
            if (savedInstanceState.getBoolean(TIMER_START)) {
                startTimer();
            } else {

                stopTimer();
            }
        }
    }





    public static TimerFragment newInstance() {
        TimerFragment fragment = new TimerFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_timer, container, false);
        unbinder = ButterKnife.bind(this, root);
        this.time = timerSaved;
        return root;
    }

    @Override
    public void onDestroyView() {


        unbinder.unbind();
        stopTimer();


        super.onDestroyView();
    }

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);
    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }

    @OnClick(R.id.timerLabel)
    public void onNumberClick() {
        if (isStarted) {
            stopTimer();
        } else {
            startTimer();
        }
    }

    @OnLongClick(R.id.timerLabel)
    public boolean onLongClick() {

        h.sendEmptyMessage(STOP);
        time = TIMER_TIME;
        timerLabel.setText(String.valueOf(time));
        return true;

    }


}
